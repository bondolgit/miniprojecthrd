

<?php  

	return [

		// Index user
		'index_title' => 'បញ្ជីអ្នកប្រើប្រាស់',
		'id' => 'ID',
		'name' => 'ឈ្មោះ',
		'email' => 'អ៊ីមែល',
		'tel' => 'លេខទូរស័ព្ទ',
		'created_at' => 'កាលបរិច្ឆេទបង្កើត',
		'profile' => 'រូបភាព',
		'action' => 'សកម្មភាព',
		'total_row_num' => 'ចំនួនអ្នកប្រើប្រាស់សរុប',
		'btn_add_new' => 'បន្ថែម​ថ្មី',
		'btn_delete_all' => 'លុប​ទាំងអស់',
		'placeholder' => 'បញ្ចូលនៅទីនេះ',
		'p_date' => 'ឆ្នាំ-ខែ-ថ្ងៃ',
		// create user
		'user_title' => 'បង្កើតអ្នកប្រើប្រាស់',
		'choose_file' => 'ជ្រើសរើសរូបភាព',
		// edit user
		'edit_title'  => 'កែប្រែអ្នកប្រើប្រាស់',
		// show user
		'user_id' => 'លេខ​សម្គាល់​អ្នក​ប្រើ',


		//Form Search
		'search_filter' => 'ស្វែងរកតាមរយៈ',
		'search_keyword' => 'ស្វែងរកពាក្យគន្លឹះ',
		'start_date' => 'ថ្ងៃ​ចាប់ផ្តើម',
		'end_date' => 'កាលបរិច្ឆេទបញ្ចប់',
		'btn_search' => 'ប៊ូតុងស្វែងរក',
		'clear' => 'ប៊ូតុងជម្រះ',
		'select_op' => 'ជ្រើសរើស',

		// Message Not found
		'not_found' => 'រកមិនឃើញទិន្នន័យ',

		// Footer
		'project_name' => 'ប្រព័ន្ធគ្រប់គ្រងអ្នកប្រើប្រាស់',
		'pro_des' => 'នេះជាការដកស្រង់ចេញពីមជ្ឈមណ្ឌលសូហ្វវែរអេសអរដាប់ធ័រកូរ៉េដែលត្រូវបានបង្កើតឡើងដោយអ៊ីអ៊ីមប៊នឌុល។ បច្ចេកវិទ្យាដែលយើងប្រើសម្រាប់បង្កើត web application គឺ HTML, CSS, jQuery, Bootstrap និង Laravel ',
		'contact_title' => 'ទាក់ទង​មក​ពួក​យើង',
		'address' => 'អាសយដ្ឋាន',
		'powerby' => 'រក្សាសិទ្ធដោយ​​ YEM BONDOL',
		
		// Number Pagination
		'0' => '០',
		'1' => '១',
		'2' => '២',
		'3' => '៣',
		'4' => '៤',
		'5' => '៥',
		'6' => '៦',
		'7' => '៧',
		'8' => '៨',
		'9' => '៩',
		'10' => '១០',

		
		// '' => '',
		// '' => '',
		// '' => '',
		// '' => '',
		// '' => '',
	];
?>
