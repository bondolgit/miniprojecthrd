

<?php  

	return [

		// Index user
		'index_title' => 'Lists Users',
		'id' => 'ID',
		'name' => 'Name',
		'email' => 'Email',
		'tel' => 'Phone Number',
		'created_at' => 'Created at',
		'profile' => 'Profile',
		'action' => 'Action',
		'total_row_num' => 'Total Row Numbers',
		'btn_add_new' => 'Add New',
		'btn_delete_all' => 'Delete All',
		'placeholder' => 'Input Here...',
		'p_date' => 'YYYY-MM-DD',
		// create user
		'user_title' => 'Create User',
		'choose_file' => 'Choose File',
		// show user
		'user_id' => 'ID',

		// edit user
		'edit_title'  => 'Edit User',

		//Form Search
		'search_filter' => 'Search Filter',
		'search_keyword' => 'Search Keyword',
		'start_date' => 'Start Date',
		'end_date' => 'End Date',
		'btn_search' => 'Search',
		'clear' => 'Clear',
		'select_op' => 'Select Option',

		// Alert Message Data Empty
		'not_found' => 'Data Not Found',

		// Footer
		'project_name' => 'User Management System',
		'pro_des' => 'This is miniproject from Korea Software HRD Center which is created by YEM BONDOL . The technology that we use for building web application are HTML , CSS , jQuery , Bootstrap , and Laravel .',
		'contact_title' => 'Contact us',
		'address' => 'Address',
		'powerby' => 'power by YEM BONDOL',

		// Number Pagination
		'0' => '0',
		'1' => '1',
		'2' => '2',
		'3' => '3',
		'4' => '4',
		'5' => '5',
		'6' => '6',
		'7' => '7',
		'8' => '8',
		'9' => '9',
		'10' => '10',
		



		// '' => '',
		// '' => '',
		// '' => '',
		// '' => '',
		// '' => '',
	];
?>
