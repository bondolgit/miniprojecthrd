<!DOCTYPE html>
<html>
<head>
	<title>Mini Project HRD</title> 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  <!-- Header -->
   


  <!-- DatePicker -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <link rel="stylesheet" type="text/css" href="{{url('css/style.css')}}">

  <!-- CKeditor -->
  <script type="text/javascript" src="{{url('ckeditor1/ckeditor.js')}}"></script> 
  <!-- <script src="{{url('js/script.js')}}"></script> -->

   <!-- Add icon library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!--CDN Sweetalert -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


  <style type="text/css">
  .container{
    margin-top: 50px;
    margin-bottom: 0 auto;
  }
  </style>

  <!-- Active Menu -->
  <script type="text/javascript">
      $(document).ready(function(){
        $("#navbar-top a").on("click", function(){
          $("#navbar-top a").removeClass("blue-navbar");
            $(this).addClass("blue-navbar");
        })
      })
  </script>

</head>
<body>
		
		@include('layout.header')

		@yield('content')


    @include('layout.top_footer')
		@include('layout.footer')
    @include('layout.bottom_footer')

		
 
	
</body>
</html> 