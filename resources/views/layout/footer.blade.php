

<footer style="background-color: #1c2331;">
	<style>
		ul,li{
			color: white;
			font-size: 15px;
		}
		h3,h2,p,span{
			color:white
		}
		/*footer margin top*/
		#ftop{
			margin-top:50px;
		}
	</style>
	<div class="container-fluid" >
		<div class="row">
			<div class="col-md-3"><br><br><br>

				<img src="{{url('./storage/FileUpload/6JMwq2pjiOSC87D84g6KXm0wVRbtD0GSCPmQqYF2.png')}}" width="100px" height="100px" class="img-thumbnail">

			</div>

			<div class="col-md-6" id="ftop">

				
				<h2> @lang('user.project_name') </h2>
				<p>@lang('user.pro_des') </p>
				
			</div>
			

			<div class="col-md-3">
				<h3 id="ftop">@lang('user.contact_title'):</h3>
				<p><b>@lang('user.address'):</b>&nbsp<span>Russey Keo,Phnom Penh</span></p>
				<p><b>@lang('user.email'):</b>  &nbsp  <span>chandara@gmail.com</span></p>
				<p><b>@lang('user.tel'): </b>   &nbsp  <span>098 234 567</span></p>
					
			</div>
		</div>

		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<p>@lang('user.powerby')</p>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>	 
</footer>