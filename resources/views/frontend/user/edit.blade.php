

@extends('layout.master')
@section('content')

<style type="text/css">
	.container{
		margin-top: 50px;  
	}
</style>
   
 	        
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="shadow p-3 mb-3 bg-white rounded">
            <form  action="{{url('/edit_user/'.$user->id)}}" method="POST" enctype="multipart/form-data">
            		{{ csrf_field() }}
           		
          		 		<header>
          		 				<h2 class="text-primary text-center">@lang('user.edit_title')</h2>
          		 		</header>
 		 
                 
                  <div class="form-group">
                    
                      <label>@lang('user.name')</label>         
                      <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
                    
                  </div>

                  

                  <div class="form-group">
                      <label> @lang('user.email') </label>         
                      <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
                 
                  </div>

                 
                  <div class="form-group">                     
                      <label> @lang('user.tel') </label> 
                      <input type="text" class="form-control" id="phone_number" name="phone_number" value="{{ $user->phone_number }}">
                  </div>

                  <!-- Profile -->
                  <div class="form-group">                     
                      <label> @lang('user.choose_file') </label> 
                      <input type="file" class="form-control" id="profile" name="profile" value="{{ $user->profile }}">

                      	@if($user->profile)

                            <img src="{{url('./storage/FileUpload'.$user->profile)}}" width="100px" height="100px">
                            
                     	  @endif

                  </div>


	               <div class="form-group">

	               		<a href="{{url('/')}}" class="btn btn-success btn-sm">
	                      <i class="fa fa-backward"></i>
	                      Back </a>
	                    <button type="submit" class="btn btn-primary btn-sm"> Submit </button>
	                    
	                 
	               </div>
            
          </form>
          </div>   <!-- close box-shadow-->
        </div>
      </div>
    </div>  <!-- close class container-->
    
@endsection
   


