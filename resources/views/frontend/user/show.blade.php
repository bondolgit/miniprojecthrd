
@extends('layout.master')
@section('content')



	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10">
			<!-- Profile -->
			<div class="">
             	@if($show_user->profile)
					<img src="{{url('./storage/FileUpload'.$show_user->profile)}}" width="150px" height="150px" class="rounded-circle float-right">
				@endif
            
            </div>
            <br><br>

		
				<table class="table table-bordered table-striped">
					<thead style="background-color: #98FB98">
                      <tr>
                        
                        <th class="text-center"><h4><b>បរិយាយ</b></h4></th>
                        <th></th>
                      </tr>
                    </thead>

                    <tbody>
						<tr>
							<td><b>@lang('user.user_id'):</b></td>
							<td>{{$show_user->id}}</td>
						</tr>

						<tr>
							<td><b>@lang('user.name'):</b></td>
							<td>{{ $show_user->name}}</td>
						</tr>

						<tr>
							<td><b>@lang('user.email'):</b></td>
							<td>{{ $show_user->email}}</td>
						</tr>

						<!-- <tr>
							<td><b>Profile</b></td>
							<td>
								@if($show_user->profile)
									<img src="{{url('./storage/FileUpload'.$show_user->profile)}}" width="70px" height="70px" class="img-thumbnail">
								@endif
							</td>
						</tr> -->
					</tbody>
				</table>
					<!-- Btn Back -->
					<a href="{{url('/')}}" class="btn btn-primary btn-sm">
						<i class="fa fa-backward"></i>
                      Back 
                  	</a>
			
			</div>
		</div>
	</div>


	
@endsection