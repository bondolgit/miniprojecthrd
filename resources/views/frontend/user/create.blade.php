
@extends('layout.master')
@section('content')

<style type="text/css"> 
	.container{
		margin-top: 50px;  
	}
</style>
   
 	        
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="shadow p-3 mb-3 bg-white rounded">
            <!-- alert error message if don't input requried field -->
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form  action="{{url('/create_user')}}" method="POST" enctype="multipart/form-data">
            		{{ csrf_field() }}
           		
          		 		<header>
          		 				<h2 class="text-primary text-center">@lang('user.user_title')</h2>
          		 		</header>
 		 
                 
                  <div class="form-group">
                    
                      <label>@lang('user.name')</label>         
                      <input type="text" class="form-control" id="name" name="name">
                    
                  </div> 

                  

                  <div class="form-group">
                      <label> @lang('user.email') </label>         
                      <input type="email" class="form-control" id="email" name="email">
                 
                  </div>

                 
                  <div class="form-group">                     
                      <label> @lang('user.tel') </label> 
                      <input type="text" class="form-control" id="phone_number" name="phone_number">
                  </div>

                  <!-- Profile -->
                  <div class="form-group">                     
                      <label> @lang('user.choose_file') </label> 
                      <input type="file" class="form-control" id="profile" name="profile">
                  </div>

                 
	               <div class="form-group">

	               		<a href="{{url('/')}}" class="btn btn-success btn-sm">
	                      <i class="fa fa-backward"></i>
	                      Back </a>
	                    <button type="submit" class="btn btn-primary btn-sm"> Submit </button>
	                    
	                 
	               </div>
            
          </form>
          </div>   <!-- close box-shadow-->
        </div>
      </div>
    </div>  <!-- close class container-->
    
@endsection
   


