
@extends('layout.master')
@section('content')

 

	<!-- Form Search --> 

	<div class="container">
	    <form action="/search" method="GET" role="search"> 
	       
	        <div class="row">

	        	<!-- Search Filter -->
	            <div class="col-md-2">
	                 <div class="form-group">
	                 	<label><b>@lang('user.search_filter')</b> </label>
	                    <select class="form-control" name="search_filter" style="background:whitesmoke;color:black;font-weight: bold">
	                    	<option value="0">---@lang('user.select_op')---</option>
	                    	@foreach($filters as $f)
	                    		<option value="{{$f}}" 

	                    			@if($f == $search_filter)
	                    				selected="selected"; 
	                    			@endif

	                    		>{{$f}}</option>
	                    	@endforeach
	                    	
	                    </select>
	                       
	                </div>
	            </div>
	            <!-- Search Keyword -->
	            <div class="col-md-3">
	                 <div class="form-group">
	                 	<label> <b>@lang('user.search_keyword')</b></label>
	                    <input type="text" class="form-control" name="keyword" placeholder="@lang('user.placeholder')" value="{{ isset($keyword) ? $keyword : ''}}">

	                       
	                </div>
	            </div>
	            <div class="col-md-1"></div>

	     		
	            <div class="col-md-3">
	            	<div class="form-group input-daterange">
	            		<label><b>@lang('user.start_date')</b></label>
	            		<input type="text" name="start_date" id="datepicker1" class="form-control" placeholder="@lang('user.p_date')" autocomplete="off" />
	            		
					 
	            	</div>

	            </div> 

	            <div class="col-md-3">
	                 <div class="form-group input-daterange">
	                 	<label><b>@lang('user.end_date')</b></label>
	                    <input type="text" name="end_date" id="datepicker2" class="form-control"  placeholder="@lang('user.p_date')"	 autocomplete="off" />
	                </div>
	            </div>

	            <div class="form-group">

	            	<button type="submit" class="btn btn-success btn-sm" name="submit_search"><i class="fa fa-search"></i>@lang('user.btn_search')</button>
	            	<button type="reset" class="btn btn-danger btn-sm">@lang('user.clear')</button>
	            	
	            </div>
	        </div>
	    </form>
	</div> 



	<!-- ======= Content ====== -->
	
	<div class="container">
		
		<h2 class="text-center text-success"><b> @lang('user.index_title')</b></h2>
		<table class="table table-bordered table-hover">
			<a href="{{url('/create_user')}}" class="btn btn-success btn-sm"><i class="fa fa-plus-circle"></i>@lang('user.btn_add_new')</a>
			<a class="btn btn-danger btn-sm delete_all"  style="float:right;color:white" onclick="sweetalert_delete_all()"><i class="fa fa-close"></i>@lang('user.btn_delete_all')</a>
			<tr style="background-color: lightgray">
				<th>ID</th>
				<th>@lang('user.name')</th>
				<th>@lang('user.email')</th>
				<th>@lang('user.tel')</th>
				<th> @lang('user.created_at')</th>
				<th>@lang('user.profile')</th>
				<th>@lang('user.action')</th>
			</tr>

			@if(count($users) > 0)	 
				@foreach($users as $u)

			<tr>
				<td>{{$u->id}}</td>
				<td>{{$u->name}}</td>
				<td>{{$u->email}}</td>
				<td>{{ $u->phone_number }}</td>
				<td>{{ date("Y-m-d",strtotime($u->created_at)) }}</td>
				<td>
					@if($u->profile)
						<img src="{{url('./storage/FileUpload'.$u->profile)}}" width="70px" height="70px" class="img-thumbnail">
					@else

					@endif
				</td>
				
				<td> 

					<a href="{{ url('/show_users/'.$u->id)}}" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a>
					<a href="{{url('/edit_user/'.$u->id)}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
					

					<button type="button" class="btn btn-danger btn_delete btn-sm" id="btn_delete" onclick="sweetalert({{$u->id}})">
						<i class="fa fa-trash"></i></button>
				

				</td>	
			</tr>

				@endforeach 

			@else
				<h2 class="text-danger">@lang('user.not_found')</h2>
				
			@endif
			 

			<tr>
			 	<td colspan="3"><b>@lang('user.total_row_num'):</b></td>
			 	<td colspan="4" class="text-center"><b style="color:red;">{{ $count }}</b></td>
			</tr>

		</table>

			<!-- Paginate -->
				<center>
					{{ $users->appends(request()->all())->links() }}
				</center>	

			
	</div>

<!-- 
	<script type="text/javascript">

		$("document").ready(function(){
			// $(".btn_delete").click(function(){
			// 	return confirm("Do you want to delete this field!!");
			// });
			//delete all rows
			// $(".delete_all").click(function(){
			// 	return confirm("Do you want to delete all records!!");
			// });
		});

		
	</script> -->


	<!-- DatePicker -->
	<script>
	   // Start Date
	  $( function() {
	    $( "#datepicker1" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
	  } );

	  // End date
	  $( function() {
	    $( "#datepicker2" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
	  } );

  </script>


  <script>
  	// Sweetalert
  	function sweetalert(id){

  		swal({
		  title: "Are you sure?",
		  text: "That you want to delete this record",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
		    swal("Poof! Your  record has been deleted!", {
		      icon: "success",
		    });
		    // js delete action
		    window.location.href = '/delete_user/'+id;
		  } else {
		    swal("Don't delete this record!");
		  }
		});
	  	}


	  	// delete all 
	  	function sweetalert_delete_all(){

  		swal({
		  title: "Are you sure?",
		  text: "That you want to delete all records",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
		    swal("Poof! Your  record has been deleted!", {
		      icon: "success",
		    });
		    // js delete action
		    window.location.href = '/delete_all';
		  } else {
		    swal("Don't delete all record!");
		  }
		});
	  	}


  </script>

@endsection






  


 