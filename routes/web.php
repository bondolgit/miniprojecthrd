<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () { 
//     return view('welcome');
// });

// Route::get('/',function(){ 
// 	return view('index');
// });

// User
Route::get('/','UserController@index')->name('index');
Route::get('/create_user','UserController@create_user')->name('create');
Route::post('/create_user','UserController@store_user')->name('create');
Route::get('/edit_user/{id}','UserController@edit_user')->name('edit');
Route::post('/edit_user/{id}','UserController@update_user')->name('edit');
Route::get('/delete_user/{id}','UserController@delete_user')->name('delete');
Route::get('/delete_all','UserController@delete_all')->name('delete_all');
Route::get('/show_users/{id}','UserController@show_user')->name('show_user');


// Search Filter & Date Range
Route::get('/search','UserController@search');

// Switch Languages
Route::get('/kh','UserController@khmer');
Route::get('/en','UserController@english');