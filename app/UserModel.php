<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    //
    public $table = "tb_user";

    // Fillable
    public $fillable = ['name','email','phone_number','profile'];
}
