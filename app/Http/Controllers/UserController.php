<?php

namespace App\Http\Controllers;   

use Illuminate\Http\Request; 
use App\UserModel;

class UserController extends Controller
{
    //index
    public function index(){ 

    	$title = "Lists Users";
        $filters = ['ID','Name','Email','Phone_Number'];
        $search_filter = "";
    	$users = UserModel::orderBy('id','DESC')->paginate(10);
        $count = UserModel::all()->count();
    		return view('frontend.user.index',compact("title","users","filters","search_filter","count"));
    }

    // Create User
    public function create_user(Request $request){

    	$title = "Create User";
    		return view('frontend.user.create',compact("title"));
    }
    // Store User
    public function store_user(Request $request){

    	$this->validate($request,[

    		'name' => 'required',
    		'email' => 'required'
    	]);

    	// // Profile 
    	$users = new UserModel();
    	$users->fill($request->all());  
    	
    	$file = $request->file("profile");
    	$destination = "public/FileUpload";   
    	$fileName = $file->store($destination);
    	// cut : public/FileUpload on profile field in tb_user
    	$users->profile = str_replace("public/FileUpload", "", $fileName);

		$users->save();
    	

    		return redirect()->route('index')->with('success','Create Successfully');
    		
    }
    // Edit User
    public function edit_user(Request $request,$id){

    	$title = "Edit User";
    	$user = UserModel::find($id);
    		return view('frontend.user.edit',compact('title','user'));
    }
    // Update User
    public function update_user(Request $request,$id){

        $user = UserModel::find($id);

    	$file = $request->file("profile");
    	$destination = "public/FileUpload";   
    	$fileName = $file->store($destination);
    	$user->profile = str_replace("public/FileUpload", "", $fileName);

    	$user->save();

    	 	return redirect()->route('index')->with('success','Updated Successfully');
    }
    // Show Users
    public function show_user($id){

        $title = "Show Users";
        $show_user = UserModel::find($id);
            return view('frontend.user.show',compact('show_user','title'));
    }

    // Delete User
    public function delete_user($id){

    	$user = UserModel::find($id);
    	$user->delete();
    		return redirect()->route('index');
    }

     // Delete All rows
    public function delete_all(){

        UserModel::truncate();
            return redirect()->back();
    }

    // Search Filter & Date Range
    public function search(Request $request){

        $title = "Lists Users";
        $keyword = $request->keyword;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $search_filter = $request->search_filter; 
        $count = 0;
        $filters = ['ID','Name','Email','Phone_Number'];



            // if unselect get default is "title" to search
            if($search_filter == "0"){
                $search_filter = 'name';
            }
       
            // search_filter condition
            if($start_date == ""){
               
                $users = UserModel::where($search_filter,'LIKE','%'.$keyword.'%')->paginate(5);
                $count = UserModel::where($search_filter,"LIKE","%".$keyword."%")->count();
            }else{
                
                $users = UserModel::where($search_filter,'LIKE','%'.$keyword.'%')
                    ->whereBetween('created_at',[$start_date,$end_date])
                    ->paginate(5);
                    
                $count = UserModel::where($search_filter,"LIKE","%".$keyword."%")
                        ->whereBetween('created_at',[$start_date,$end_date])
                        ->count();
                        
            }

        // count row per page
        $count_row = $users->count();

           return view('frontend.user.index',compact("title","users","filters","keyword","start_date","end_date","search_filter","count","count_row"));
    }


    // khmer 
    public function khmer(){
        \App::setlocale("kh");
        \Session::put("locale","kh");
            return redirect()->back();
    }

    // English
    public function english(){
        \App::setlocale("en");
        \Session::put("locale","en");
           return redirect()->back();  
    }

    

}
